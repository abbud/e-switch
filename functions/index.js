'use strict';
const functions  = require('firebase-functions');
const nodemailer = require('nodemailer');
const cors = require('cors')({origin: true});

let url = "smtps://dontreply.e.switch%40gmail.com:"+encodeURIComponent('chicothecat') + "@smtp.gmail.com:465";
let transporter = nodemailer.createTransport(url);

exports.enviarEmail = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const { from, to, subject, text, html } = req.body
    const email = {
        from,
        to,
        subject,
        text,
        html
    };

    transporter.sendMail(email, (error, info) => {
        if (error) {
          return console.log(error);
        }
      else{
        res.status(200).send();
      }
    });
  });
});
