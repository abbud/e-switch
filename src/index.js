import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from 'store';
import { Provider } from 'react-redux'
import App from './App';
import * as serviceWorker from './serviceWorker';

const store = configureStore()

const Application= () => (
    <Provider store={store}>
        <App />
    </Provider>
);

ReactDOM.render(
    <Application/>,
    document.getElementById('root'));

serviceWorker.register();
