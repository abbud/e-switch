import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Logo from 'Logo';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';

const StyledTypography = styled(Typography)`
  &&&{
margin:0 5px;
}
`

const AvatarContainer = styled.div`
  &&&{
    display:flex;
    flex-direction:columns;
    justify-content:space-between;
    align-items:center;
  }
`

const LogoContainer = styled.div`
  &&&{
  display:flex;
  flex-direction:columns;
  justify-content:center;
  align-items:center;
}
`;
const StyledToolBar = styled(Toolbar)`
  &&&&{
  display:flex;
  justify-content:space-between;
}
`;

const NamedAvatar = ({profile }) => profile
  ?(
    <AvatarContainer> 
      <StyledTypography
        color='secondary'>
        {profile.displayName}
      </StyledTypography>
      {profile.avatarUrl
      ?<Avatar src={profile.avatarUrl}/>
        :null}
    </AvatarContainer>)
  :null

const CustomAppBar = ({profile}) =>
  (
    <AppBar position="sticky">
      <StyledToolBar>
        <LogoContainer>
          <Logo/>          
    <StyledTypography 
      variant='h6'
      color='secondary'>
      E-Switch
    </StyledTypography>
    </LogoContainer>
    <NamedAvatar profile={profile}/>
      </StyledToolBar>
    </AppBar>
  );

const mapFirebaseStateToProps = state => ({
  profile: state.firebase.profile,
})

export default connect(mapFirebaseStateToProps)(CustomAppBar)
