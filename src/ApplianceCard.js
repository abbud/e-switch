import React from 'react';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';

const StyledCard = styled(Card)`
  &&&{
    min-width:125px;
    min-height:125px;
    max-height:350px;
    max-width:350px;
    display:flex;
    margin:8px;
    padding:8px;
    justify-content:center;
    align-items:center;
  }
`;


const SimpleCard = props => {
  return (
    <StyledCard
    onClick={props.onClick}>
    <Typography 
      variant='h6'
      color="textSecondary" gutterBottom>
          {props.name}
        </Typography>
    </StyledCard>
  );
}

export default SimpleCard;
