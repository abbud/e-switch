import React from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import { withFirebase } from 'react-redux-firebase'
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

const OnButton = styled(Button)`
  &&&{
    background-color:${green[500]}
    color:white;
  }
`;
const OffButton = styled(Button)`
  &&&{
    background-color:${red[500]}
    color:white;
  }
`;




const emailSendUrl = 'https://us-central1-e-switch.cloudfunctions.net/enviarEmail';

const ApplianceDialog = ({
  handleClose, appliance,
  open, firebase: { push },
  auth, requestSentSnackbar,
  location
}) => {
  const requestTurn = (turnState)=> {
    const newRequest = {
      ...appliance,
      'action':`Turn ${turnState}`,
      timeStamp:Date.now()
    }
    const turnStateMap = {
      'On':'ligar',
      'Off':'desligar',
    }
    handleClose()
    push(`requests/${auth.uid}`, newRequest)

    const dateNow = new Date()
    const text = `${auth.displayName} fez uma solicitação para ${turnStateMap[turnState]} ${appliance.name}.
Observações do dispostivo: ${appliance.obs}.
Departamento: ${location.department}.
Bloco: ${location.block}.
Sala: ${location.room}.
Solicitação foi feita às ${dateNow.toLocaleString()}`

    Axios.post(emailSendUrl,({
      from: '"E-switch" <dontreply.e.switch@gmail.com>',
      to:'cjsantos86@gmail.com',
      subject:`Pedido de ${auth.displayName} para ${turnStateMap[turnState]} ${appliance.name}`,
      text,
    }))
    requestSentSnackbar();
  }
  return(
    <Dialog
      onClose={handleClose} 
      open={open}
    >
      <DialogTitle>
        {appliance && appliance.name}
      </DialogTitle>
      <DialogActions>
        <OnButton 
          variant='contained'
          onClick={() => requestTurn('On')}
        >
          Ligar
        </OnButton>
        <OffButton 
          variant='contained'
          onClick={() =>requestTurn('Off')}
        >
          Desligar 
        </OffButton>
      </DialogActions>
    </Dialog>
  )
};

ApplianceDialog.defaultProps = {
  appliance: {
    name:'',
  },
  auth:{
    name:null,
    uid:null,
  },
  location:{
    block:'',
    room:'',
    department:'',
  }
}

ApplianceDialog.propTypes = {
  appliance: PropTypes.object,
  auth:PropTypes.object,
  location:PropTypes.object,
}

export default withFirebase(ApplianceDialog);
