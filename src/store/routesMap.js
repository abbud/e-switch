const routesMap = {
  HOME: '/',
  LOGIN: '/login',
  FIRST_TIME: '/first-time',
};

export default routesMap;
