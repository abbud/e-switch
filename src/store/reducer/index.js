import * as mockReducers from './mock_reducer';

const reducers = {
  ...mockReducers,
};

export default reducers;


