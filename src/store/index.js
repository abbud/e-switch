import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { firebaseReducer, reactReduxFirebase  } from 'react-redux-firebase';
import { connectRoutes } from 'redux-first-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import firebase from 'firebase';

import routesMap from './routesMap';
import appReducers from 'store/reducer';

const firebaseConfig = {
    apiKey: "AIzaSyD1n_pLr8CTDNLPnfWJB99vyeVGwGsK-lE",
    authDomain: "e-switch.firebaseapp.com",
    databaseURL: "https://e-switch.firebaseio.com",
    projectId: "e-switch",
    storageBucket: "e-switch.appspot.com",
    messagingSenderId: "1016921020858"
  };

firebase.initializeApp(firebaseConfig)

const config = {
  userProfile: 'users',
  enableLogging: false,
}



const configureStore = (...args) => {
  const { middleware, enhancer, reducer } = connectRoutes(
    routesMap,
  );
  const middlewares = applyMiddleware(middleware, thunk, ...args);
  const rootReducer = combineReducers({
    ...appReducers,
    firebase: firebaseReducer,
    location: reducer,
  });
  const enhancers = composeWithDevTools(enhancer,reactReduxFirebase(firebase, config), middlewares)
  return createStore(
    rootReducer,
    enhancers,
  )
}

export default configureStore;
