import React from 'react'
import styled from 'styled-components';
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase';
import GoogleButton from 'react-google-button';

const FlexContainer = styled.div`
  margin:10%;
  display:flex;
  justify-content:center;
  align-items:center;
`;

const LoginPage = ({ firebase, auth, backToHome, registerLocation, profile }) => {
  if(!auth.isEmpty && !!auth.isLoaded && !!profile.isLoaded){
    if(!profile.location){
      registerLocation()
    }
    else{
      backToHome()
    }
  }
  return(
  <FlexContainer>
    <GoogleButton
      onClick={() => firebase.login({ provider: 'google', type: 'popup' })}
    />
  </FlexContainer>
)
}

LoginPage.propTypes = {
  firebase: PropTypes.shape({
    login: PropTypes.func.isRequired
  }),
  auth: PropTypes.object
}

const mapDispatchToProps = (dispatch) =>  ({
  backToHome: () => dispatch({type:'HOME'}),
  registerLocation: () => dispatch({type:'FIRST_TIME'})
})

export default compose(
  firebaseConnect(), // withFirebase can also be used
  connect(({ firebase: { auth, profile } }) => ({ auth, profile }), mapDispatchToProps)
)(LoginPage);
