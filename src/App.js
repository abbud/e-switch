import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import { connect }from 'react-redux';
import { NOT_FOUND } from 'redux-first-router';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import Appliances from './Appliances';
import LoginPage from './LoginPage';
import CustomAppBar from './CustomAppBar';
import RegisterLocation from './RegisterLocation';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#60ac5d',
      main: '#2e7c31',
      dark: '#004f04',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ffffff',
      main: '#f5f5f5',
      dark: '#c2c2c2',
      contrastText: '#000',
    },
  },
});
const componentMap ={
  LOGIN: <LoginPage/>,
  FIRST_TIME: <RegisterLocation/>,
  HOME: <Appliances/>,
    [NOT_FOUND]: <div> Not found </div>,
};

const App =  ({location})  =>( 
  <MuiThemeProvider theme={theme}>
    <React.Fragment>
      <CssBaseline/>
  <CustomAppBar/>
  { componentMap[location] || componentMap[NOT_FOUND] }
  </React.Fragment>
  </MuiThemeProvider>
)


const mapStateToProps = state => ({
  location: state.location.type,
});

App.defaultProps = {
  location: 'HOME',
};
App.propTypes = {
  location: PropTypes.string,
};

export default connect(mapStateToProps, null)(App);
