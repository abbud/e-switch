import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isLoaded, isEmpty } from 'react-redux-firebase';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Zoom from '@material-ui/core/Zoom';
import AddIcon from '@material-ui/icons/Add';
import ApplianceCard from './ApplianceCard'
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';

import NewApplianceModal from './NewApplianceModal';
import AppliancesDialog from './ApplianceDialog';


const CenterPage = styled.div`
  &&{
    position:absolute;
    display:flex;
    height:100%;
    width:100%;
    justify-content:center;
    align-items:center;
  }
`;

const AbsoluteButton = styled(Button)`
  &&{
    position:fixed;
    bottom:8px;
    right:8px;
  } 
`;

const ContentContainer = styled.div`
  &&{
    margin:12px 2.5%;
    display:flex;
    flex-wrap:wrap;
    justify-content:center;
    align-items:center;
    flex-direction:row;
  }
`;

const AppliancesContainer = styled.div`
  &&{
    display:flex;
    flex-direction:columns;
    flex-wrap:wrap;
    justify-content:space-around;
    align-items:center;
  }
`;

const RequestSentToast = ({open, onClose,autoHideDuration}) => (
  <Snackbar
    anchorOrigin={{ vertical:'bottom', horizontal:'right' }}
    open={open}
    autoHideDuration={autoHideDuration}
    onClose={onClose}
    ContentProps={{
      'aria-describedby': 'message-id',
    }}
    message={<span id="message-id">Solicitação enviada!</span>}
  />
)


class Appliances extends Component {
  static propTypes = {
    authExists: PropTypes.bool,
  };
  state = {
    selectedApplianceKey:null,
    newApplianceModalOpen: false,
    openToast: false,
  }
  componentWillReceiveProps({ auth }) {
    if(!checkAuthState(auth)) {
      this.props.loginRedirect()
    }
  }
  handleCloseSnackBar = () => {
    this.setState({openToast:false})
    }


  handleModalClose = () => this.setState({newApplianceModalOpen:false})
  handleModalOpen = () => this.setState({newApplianceModalOpen:true})
  handleDialogClose = () => this.setState({selectedApplianceKey:null})
  selectAppliance = key => this.setState({selectedApplianceKey:key})

  addButton = (
    <Zoom
      in
      unmountOnExit
    >
      <AbsoluteButton 
        onClick={this.handleModalOpen}
        color='primary'
        variant="fab">
        <AddIcon/>
    </AbsoluteButton>
    </Zoom>
  )


  render(){
    const { appliances } = this.props;
    const { newApplianceModalOpen, selectedApplianceKey, openToast } = this.state;
    const appliancesList = appliances?Object.keys(appliances).map(
      (key, id) => (
        <ApplianceCard 
          key={key}
          id={id}
          name={appliances[key]['name']}
          onClick={() => this.selectAppliance(key)}
        />
      )
    ):null
    
    return (!isLoaded(appliances) & !isEmpty(appliances))? <CenterPage><CircularProgress/></CenterPage>
      :(<React.Fragment>
        <div>
          <Typography 
            gutterBottom 
            variant='h5'>Dispositivos</Typography>
          <ContentContainer>
            <AppliancesContainer>
              {appliancesList}
            </AppliancesContainer>
          </ContentContainer>
        </div>
        <NewApplianceModal
          modalOpen={newApplianceModalOpen}
          onClose={this.handleModalClose}
        />
        <AppliancesDialog
          open={!!selectedApplianceKey}
          auth={this.props.auth}
          handleClose={this.handleDialogClose}
          appliance={appliances && appliances[selectedApplianceKey]}
          requestSentSnackbar={() => this.setState({openToast:true})}
          location={this.props.location}
        />
        {this.addButton}
        <RequestSentToast
          open={openToast}
          autoHideDuration={5000}
          onClose={this.handleCloseSnackBar}/>
        </React.Fragment>)}
}

const checkAuthState = auth =>  !!auth && !!auth.isLoaded && !!auth.uid

const mapDispatchToProps = dispatch => ({
  loginRedirect: () => dispatch({type:'LOGIN'})
});

const mapFirebaseStateToProps = state => ({
  appliances: state.firebase.profile.appliances,
  auth: state.firebase.auth,
  location: state.firebase.profile.location,
}) 

export default connect(mapFirebaseStateToProps, mapDispatchToProps)(Appliances)
