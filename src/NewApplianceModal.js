import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { firebaseConnect } from 'react-redux-firebase'
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';

const ContentContainer = styled.div`
  &&{
    margin:12px 2.5%;
    display:flex;
    flex-wrap:wrap;
    justify-content:center;
    align-items:center;
    flex-direction:row;
  }
`;

const ModalPaper = styled(Paper)`
  &&&{
    position:absolute;
    display:flex;
    flex-direction:rows;
    justify-content:space-around;
    flex-wrap:wrap;
    width:75%;
    padding:20px;
    top: 50%;
    left:50%;
    transform: translate(-50%, -50%);
  }
`

const initialState = {
  name:'',
  obs:'',
}

class NewApplianceModal extends React.Component {
  state = {
    ...initialState
  }

  handleNewApplianceChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  registerNewAppliance = (appliance) => {
    if(appliance.name){
      const user = this.props.auth.uid;
      this.props.firebase.database().ref(`users/${user}/appliances`).push(appliance)
      this.props.onClose()
      this.setState(initialState)
    }
  }

  render(){
    const {modalOpen, onClose } = this.props
    const { name, obs } = this.state
    return(
      <Modal 
        open={modalOpen}
        onClose={onClose}>
        <ModalPaper>
          <ContentContainer>
              <Typography 
                gutterBottom 
                variant='h6'>
                Adicionar novo dispositivo
              </Typography>
              <TextField
                required
                value={name}
                onChange={this.handleNewApplianceChange('name')}
                label='Nome'
              /> 
      <TextField
        value={obs}
        onChange={this.handleNewApplianceChange('obs')}
        label='Observações'/>
      </ContentContainer>
      <Button
        variant='contained'
        color='primary'
        onClick={() => this.registerNewAppliance({...this.state})}
        fullWidth 
      >
        <AddIcon/>Adicionar
      </Button>
      </ModalPaper>
      </Modal>
    )
  }
}

NewApplianceModal.propTypes ={
  modalOpen:PropTypes.bool,
  onClose:PropTypes.func,
}

NewApplianceModal.defaulProps ={
  modalOpen:false,
  onClose: () =>null,
}

const mapFirebaseStateToProps = (state) => ({ 
  auth:state.firebase.auth
})

export default compose(
  firebaseConnect(),
  connect(mapFirebaseStateToProps)
)(NewApplianceModal)
