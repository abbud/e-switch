import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import styled from 'styled-components';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';


const services = `Nossa missão é dar às pessoas o poder de gerenciar sua casa a distância conectando o
mundo a elas. Para ajudar a avançar nessa missão, fornecemos os produtos e serviços
descritos abaixo para você:`

const experience =`Forneça uma experiência personalizada para você:
Sua experiência no E-Switch é diferente da de qualquer outra pessoa: aproximamos sua casa,
seus aparelhos eletroeletrônicos de você, assim trazendo maior segurança e controle sobre as
situações quando não se está presente`

const weEnable = `Há muitas maneiras de se poupar energia e uma delas é tendo uma maior visão sobre os
gastos que sua casa vem trazendo. Levamos até você uma análise simples e concisa dos seus
gastos com energia, discriminado por tomadas ao longo dos cômodos ou setores da casa.
Ajudá-lo a descobrir qual equipamento gasta mais:`;

const weHelp = `Mostramos o consumo de energia elétrica dividido por tomadas será possível fazer uma
detecção fina dos aparelhos que mais gastam dentro de sua casa, assim em uma eventual
troca você poderá confrontar as características do aparelho já possuído com o aparelho a ser
comprado. Desse modo podemos oferecer mais uma fonte de corte de gastos.
Use tecnologias avançadas para fornecer serviços seguros e
funcionais para todos:`

const weUse = `Usamos e desenvolvemos tecnologias avançadas para que as pessoas possam usar nossos
produtos com segurança, independentemente da capacidade física ou localização geográfica.
Habilite o acesso global aos nossos serviços:`

const globalNeeds = `Para operar nosso serviço global, precisamos armazenar e distribuir análise de dados em
nosso app. E para esse uso é preciso que sua infraestrutura conte com uma instalação bem
feita de nossas tomadas inteligentes, contacte-nos para saber quem é o representante mais
proximo de você.`

const departmentOptions = [
  { value: 'ICMC', label: 'ICMC'},
  { value: 'EESC', label: 'EESC'},
  { value: 'IFSC', label: 'IFSC'},
  { value: 'IQSC', label: 'IQSC'},
  { value: 'IAU', label: 'IAU'},
];

const Container = styled.div`
  margin:25% 12.5%;
  display:flex;
  justify-content:center;
  flex-direction:column;
`

const CenteredTypography = styled(Typography)`
  &&&{
    margin-top:24px;
  text-align:center;
  }
  `

const StyledButton = styled(Button)`
  &&&{
  margin-top:24px;
  }
  `

class RegisterLocation extends React.Component {
  state={
    open:true,
    department:'',
    block:'',
    room:'',
    buttonDisabled:false,
  }

  handleClose = () => this.setState({open:false})
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleDepartmentSelect = (department) => {
    this.setState({department})
  }

  handleRegister = ({auth, firebase, profile}) => {
    this.setState({buttonDisabled:true})
    const locationInfo = {
      department:this.state.department.value,
      room:this.state.room,
      block:this.state.block,
    }
    firebase.database().ref(`users/${auth.uid}/location`).set(locationInfo, (error) =>{
      if(error){
        this.setState({buttonDisabled:false})
      }
      else{
        this.props.backToHome()
      }
    })
  }
  render(){
    const {auth, backToHome, backToLogin} = this.props
    if(!!auth.isLoaded && !auth.isEmpty){
      if(!!this.props.profile.location){
        backToHome()
      }
    }
    else{
      backToLogin()
    }
    const content = (
      <div>
        <CenteredTypography variant="headline"> Primeiro acesso </CenteredTypography>
      <Container>
        <Select
          label="Departamento"
          options={departmentOptions}
          onChange={this.handleDepartmentSelect}
          value={this.state.department}/>
        <TextField
          label="Bloco"
          onChange={this.handleChange('block')}
          value={this.state.block}/>
      <TextField 
        label="Sala"
        onChange={this.handleChange('room')}
        value={this.state.room}/>
      <StyledButton
        variant='contained'
        onClick={() => this.handleRegister(this.props)}
        disabled={this.state.buttonDisabled}
        color='primary'>
        Cadastrar 
      </StyledButton>

        <Dialog
        open={this.state.open}>
          <DialogTitle id="scroll-dialog-title">Termos de Serviço</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <Typography variant="body1" gutterBottom>
                {services}
              </Typography>
              <Typography variant='title' gutterBottom>
                Forneça uma experiência personalizada para você:
              </Typography>
              <Typography variant="body1" gutterBottom>
                {experience}
              </Typography>
              <Typography variant='title' gutterBottom>
                Capacitamos você a controlar os gasto com energia melhor:
              </Typography>
              <Typography variant="body1" gutterBottom>
                {weEnable}
              </Typography>
              <Typography variant='title' gutterBottom>
                Ajudá-lo a descobrir qual equipamento gasta mais:
              </Typography>
              <Typography variant="body1" gutterBottom>
                {weHelp}
              </Typography>
              <Typography variant='title' gutterBottom>
                Use tecnologias avançadas para fornecer serviços seguros e
                funcionais para todos:
              </Typography>
              <Typography variant="body1" gutterBottom>
                {weUse}
              </Typography>
              <Typography variant='title' gutterBottom>
                Habilite o acesso global aos nossos serviços:
              </Typography>
              <Typography variant="body1" gutterBottom>
                {globalNeeds}
              </Typography>

            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}
              color="primary"
              variant='contained'
            >
           Concordo com os termos de uso
         </Button>
       </DialogActions>
     </Dialog>
      </Container>
      </div>
    )
    return !!this.props.profile.isLoaded?content:null
  }
}

const mapFirebaseStateToProps = (state) => ({ 
  profile:state.firebase.profile,
  auth:state.firebase.auth
})

const mapDispatchToProps = dispatch => ({
  backToHome: () => dispatch({type:'HOME'}),
  backToLogin: () => dispatch({type:'LOGIN'}),
})

export default compose(
  firebaseConnect(),
  connect(mapFirebaseStateToProps, mapDispatchToProps)
)(RegisterLocation);
